package uz.pdp.model;

public class Printer {
    private int catridgeSize;
    private int tonerVolume; //toner hajmi
    private String tonerColor;
    private double tonerUsage;
    private boolean switchOn;

    public void print(String text){
        if (switchOn){
            if (text.length()*tonerUsage<=tonerVolume){
                System.out.println(text);
                this.tonerVolume -= text.length()*tonerUsage;
            }else {
                int temp = (int)(tonerVolume/tonerUsage);
                System.out.println(text.substring(0,temp));
                this.tonerVolume = 0;
                System.out.println("Toner tugadi!");
            }
        }
    }

    public void reFill(int ink){
        if (tonerVolume+ink<=catridgeSize){
            this.tonerVolume += ink;
            System.out.println(ink+" miqdorda toner quyildi");
        }else {
            this.tonerVolume = catridgeSize;
            System.out.println(catridgeSize-tonerVolume);
        }
    }

    public void turn(){
        this.switchOn = !switchOn;
    }


    public Printer() {
        this.catridgeSize = 100;
        this.tonerVolume = 70;
        this.tonerColor = "Black";
        this.tonerUsage = 0.1;
    }

    public Printer(int catridgeSize, int tonerVolume, String tonerColor, double tonerUsage, boolean switchOn) {
        this.catridgeSize = catridgeSize;
        this.tonerVolume = tonerVolume;
        this.tonerColor = tonerColor;
        this.tonerUsage = tonerUsage;
        this.switchOn = switchOn;
    }

    public int getCatridgeSize() {
        return catridgeSize;
    }

    public void setCatridgeSize(int catridgeSize) {
        this.catridgeSize = catridgeSize;
    }

    public int getTonerVolume() {
        return tonerVolume;
    }

    public void setTonerVolume(int tonerVolume) {
        this.tonerVolume = tonerVolume;
    }

    public String getTonerColor() {
        return tonerColor;
    }

    public void setTonerColor(String tonerColor) {
        this.tonerColor = tonerColor;
    }

    public double getTonerUsage() {
        return tonerUsage;
    }

    public void setTonerUsage(double tonerUsage) {
        this.tonerUsage = tonerUsage;
    }

    public boolean isSwitchOn() {
        return switchOn;
    }

    public void setSwitchOn(boolean switchOn) {
        this.switchOn = switchOn;
    }

    @Override
    public String toString() {
        return "Printer{" +
                "catridgeSize=" + catridgeSize +
                ", tonerVolume=" + tonerVolume +
                ", tonerColor='" + tonerColor + '\'' +
                ", tonerUsage=" + tonerUsage +
                ", switchOn=" + switchOn +
                '}';
    }
}

//nechi gram quyilishini hisoblash kerak ????
//probellarni olib tashlab printerdan chiqaradigan funksiya yozish kerak???
// kraska miqdorini ko'rish menuga qo'shish kerak???