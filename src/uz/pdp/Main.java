package uz.pdp;

import uz.pdp.model.Printer;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Printer epson = new Printer();

        System.out.println("1 => Continue, 0 = exit");
        System.out.print("Menuga utishga(1)ni bosing: ");
        int stepCode =scanner.nextInt();
        while (stepCode !=0){

            System.out.println("1=> CHECK SWITCH, 2=> ON/OFF, 3=> PRINT, " +
                    "4=> REFILL(zapravka), 5=> TONER AMOUNT, 6=> INFO");
            System.out.print("Menudagi hizmatlarni kiriting: ");
            int menu= scanner.nextInt();
            switch (menu){
                case 1:
                    if (epson.isSwitchOn()){
                        System.out.println("Printer yoqilgan!");
                    }else {
                        System.out.println("Printer o'chiq!");
                    }
                    break;
                case 2:
                    epson.turn();
                    break;
                case 3:
                    epson.print("Chiqariladigan text matnni ekirandan kiritadigan qilgin!!!");
                    break;
                case 4:
                    System.out.print("Quyiladigan Toner miqdorini kiriting: ");
                    epson.reFill(scanner.nextInt());
                    break;
                case 5:
                    System.out.println("kraska miqdorini ko'rish menuni qo'shgin. dankasalik qilmasdan!!!");
                    break;
                case 6:
                    System.out.println(epson.toString());
                    break;
                    default:
                        System.out.println("Menuda bor sonlarni kiriting!!!");
            }


            System.out.println("1 => Continue, 0 = exit");
            stepCode =scanner.nextInt();
        }

    }
}
